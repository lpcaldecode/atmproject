/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package atmproject;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author 16150
 */
public class ATMProject {

  
    private static final JLabel iconATM= new JLabel(new ImageIcon(ATMProject.class.getResource("/resources/atmicon.png")));
    private static JLabel welcomeLabel;
    private static JButton btnLogin;
    
    public static JPanel jPanelIcon =new JPanel();
    
    private static JPanel jPanelWelcome;
    private static JPanel jPanelButton;
    
    
    public static void main(String[] args) {
           
        int with=500;
        int height=300;
        final JFrame frame = new JFrame("ATM System By 16150");       
        frame.setSize(with, height);
       
        welcomeLabel= new JLabel("<html><center>Welcome to ATM System" + "</center></html>",JLabel.CENTER);      
        welcomeLabel.setMinimumSize(new Dimension(with/2, height/2));
        
        btnLogin = new JButton("Login");      
       
 
        btnLogin.addActionListener(
                new ActionListener(){
                    public void actionPerformed(ActionEvent e) {
                        LoginDialog loginDlg = new LoginDialog(frame);
                        loginDlg.setVisible(true);
                        if(loginDlg.isSucceeded()){
                            btnLogin.setText("Hi " + loginDlg.getUsername() + "!");
                            System.out.println("Welcome..." + loginDlg.getUsername() + "!");
                            System.out.println("Your current in the system to check your current balance...");
                           
                             new  ATMFrame(loginDlg.getUsername() );
                         
                        }
                    }
                });
        
   
        
        
        jPanelWelcome = new JPanel();
        jPanelWelcome.setLayout(new BoxLayout(jPanelWelcome, BoxLayout.Y_AXIS)); 
        jPanelWelcome.add(welcomeLabel);
        
        jPanelButton = new JPanel();
        jPanelButton.add(btnLogin); 
        
        jPanelWelcome.add(jPanelButton);
        
        jPanelIcon.add(iconATM);
        
        frame.setLayout(new FlowLayout());
        frame.add(jPanelIcon, BorderLayout.WEST);
        frame.add(jPanelWelcome, BorderLayout.EAST);
 
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
          
        frame.setVisible(true);
        
    }
    
    
    
   
}
