package atmproject;

/* Convert. 

Class to perform some standard data conversions - String to a numeric data type

usage;

int i = Convert.to_int(String) ;
float f = Convert.to_float(String) ;

*/

final class Convert {

public static byte to_byte(String s) {
	try { 
   	byte b = Byte.parseByte(s) ; 
		return b ;
	}  catch (NumberFormatException e) { 
   	displayError(s,"byte") ; 
		return 0 ;
	}
} // end to_byte

public static short to_short(String s) {
	try { 
   	short si = Short.parseShort(s) ; 
		return si ;
	}  catch (NumberFormatException e) { 
   	displayError(s,"short") ; 
		return 0 ;
	}
} // end to_short

public static String to_lower(String s) {
	
   	String si = s.toLowerCase() ; 
	return si ;
	
	
} //
public static int to_int(String s) {
	try { 
   	int i = Integer.parseInt(s) ; 
		return i ;
	}  catch (NumberFormatException e) { 
   	displayError(s,"int") ; 
		return 0 ;
	}
} // end to_int

public static long to_long(String s) {
	try { 
   	long l = Long.parseLong(s) ; 
		return l ;
	}  catch (NumberFormatException e) { 
   	displayError(s,"long") ;
		return 0 ; 
	}	
} // end to_long

public static float to_float(String s) {
	try { 
   	float f = Float.parseFloat(s) ; 
		return f ;
	}  catch (NumberFormatException e) { 
   	displayError(s,"float") ; 
		return 0 ;
	} 
} // end to_float

public static double to_double(String s) {
	try { 
   	double d = Double.parseDouble(s) ; 
		return d ;
	}  catch (NumberFormatException e) { 
   	displayError(s,"double") ; 
		return 0 ;
	} 
} // end to_double

public static char to_char(String s) { 
   	char c = s.trim().charAt(0)  ; 
		return c ;
} // end to_char

private static void displayError(String s, String t) {
	System.err.println("Error converting " + s + " to " + t) ;
} // end displayError

} // end class