package atmproject;

import javax.swing.*;import java.awt.*;import java.awt.event.*;

public class DisplayDialog extends JDialog implements ActionListener{
     
     JButton btnCancel=new JButton("Close");

     JTextArea txtDisplay= new JTextArea();
    
     JPanel main=new JPanel();
   
     public DisplayDialog(Frame parent,String title,boolean modal){
            super(parent,title,modal); 

            main.setLayout(new BorderLayout());
            main.add(new JLabel("Account Details",JLabel.CENTER),BorderLayout.NORTH);
            main.add(txtDisplay,BorderLayout.CENTER);

            JPanel south=new JPanel();
            south.add(btnCancel);
            btnCancel.addActionListener(this);
            main.add(south,BorderLayout.SOUTH); 
            
            setContentPane(main);           
            setSize(400,650);//setSize(450,200);            
     }


     public void initDisplayArea(String output){
            txtDisplay.setText("");
            txtDisplay.setText(output); 
     }

     public void actionPerformed(ActionEvent evt){
         setVisible(false);
         this.dispose();
     }

   
}
