package atmproject;

import java.util.*;import java.io.* ;
import java.util.logging.Level;
import java.util.logging.Logger;
public class Banking {
  
   
  
   

    
    public static boolean addNewAccount(String user,String title,double deposit,boolean available){
      
        try {
            Account aAccount=new Account(user,title,deposit,available);
            
            Writer output = new BufferedWriter(new FileWriter("src/atmproject/account.csv",true));  //clears file every time
            output.append(aAccount.getRow());
            output.close();

            return true;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Banking.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Banking.class.getName()).log(Level.SEVERE, null, ex);
        } 
       return false;
    }
        

    public static Vector showAllAccounts()   {
        
          Vector accounts=new Vector(); 
          try  {
              
              
                BufferedReader inBuffer = new BufferedReader(  new FileReader("src/atmproject/account.csv") ) ;
                while (true){
                    String line = inBuffer.readLine( ) ;   
                    if (line == null) {
                        break ;
                    }
                    StringTokenizer st = new StringTokenizer(line,",") ;     
                    String cId = st.nextToken( ) ; 
                    String title = st.nextToken( ) ; 
                    String sDeposit = st.nextToken( ) ; double deposit=Convert.to_double(sDeposit);
                    String sActive = st.nextToken( ) ;  String lowercaseActive = Convert.to_lower(sActive);
                    Boolean active = Boolean.valueOf(lowercaseActive); 
                    accounts.add(new Account(cId,title,deposit,active));
               }
            inBuffer.close( ) ; 
            
          } catch (IOException e) {
             System.out.println(e) ;
          }
       
          return accounts;
        }
    }
