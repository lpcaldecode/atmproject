package atmproject;

import javax.swing.*; 
import java.awt.*;
import java.awt.event.*;

public class NewDialog extends JDialog implements ActionListener{
     
    JButton btnOk=new JButton("Add");
    JButton btnCancel=new JButton("Close");

    JTextField txtUser = new JTextField();
    JTextField txtTitle= new JTextField();
    JTextField txtAuthor= new JTextField();
    JTextField txtAvail= new JTextField();

     JComboBox cboAvailability=new JComboBox(new String[]{"True","False"});
     
     JPanel main=new JPanel();

	 public NewDialog(Frame parent,String title,boolean modal){
            super(parent,"New Account - "+title,modal); 

	    main.setLayout(new GridLayout(5,2,30,30));

            main.add(new JLabel("User ID",JLabel.RIGHT));
            main.add(txtUser); 
            txtUser.setText(title);
            txtUser.setEditable(false);
            txtUser.setBorder(null);
            main.add(new JLabel("Account Name",JLabel.RIGHT));
            main.add(txtTitle); 
            main.add(new JLabel("Initial Amount",JLabel.RIGHT));
            main.add(txtAuthor); 
            main.add(new JLabel("Active",JLabel.RIGHT));
            main.add(cboAvailability);
            main.add(btnOk); 
            main.add(btnCancel); 

            btnOk.addActionListener(this);
            btnCancel.addActionListener(this);

            setContentPane(main);

            setSize(300,350);
            setVisible(true);
	 }

     public void actionPerformed(ActionEvent evt){
         if(evt.getSource()==btnOk){
			String user=txtUser.getText();
			String title=txtTitle.getText();
			String txtAmount=txtAuthor.getText();
			double amount = Convert.to_double(txtAmount);
			String status=(String)cboAvailability.getSelectedItem();

			boolean available=false;
                        if(status.equalsIgnoreCase("True")){
                            available=true;	
                        }
                        
                        setVisible(false);
			
                        
                        boolean result=Banking.addNewAccount(user,title,amount,available);
			
                        
                        JOptionPane.showMessageDialog(this.getParent(),
                        result ? "Account added!" : "Unable to add account",
                        "New Account",
                        result ? JOptionPane.INFORMATION_MESSAGE:JOptionPane.ERROR_MESSAGE);
                        
                        this.dispose();
			
		 }else{
			setVisible(false);
			this.dispose();
                }
	 }
     
}

