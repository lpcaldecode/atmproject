package atmproject;

public class Account{
	String userid;
	String title;
	double deposit;
	boolean available;

	public Account(String userid,String title,double deposit,boolean available){
          this.userid=userid;
		  this.title=title;
		  this.deposit=deposit;
		  this.available=available;
	}

    public String getUserid(){
	       return userid;	}

    public boolean isAvailable(){
		return available;	}

    public void setAvailable(boolean status){
		available=status;	}

	public String checkAvailability(){
	       boolean status=isAvailable();
		   if(status==true)
			   return " On Active";
		   else
			   return " No Active";
	}

    public String toString(){
	   return "\n" + title + " account: $" + deposit + " " + checkAvailability();
	}
    public String getRow(){
        return userid+","+title+","+deposit+","+(available?"TRUE":"FALSE");
    }

    public void printMe(){
	   System.out.println(this);	
    }

    
}

