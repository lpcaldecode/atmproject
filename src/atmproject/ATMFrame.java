package atmproject;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class ATMFrame extends JFrame implements ActionListener{
     
     private JButton btnNewAccount=new JButton("Add New Account");
     private JButton btnShowAll=new JButton("Display Account");
     private JButton btnExit=new JButton("Exit");
     private JPanel main=new JPanel();
     private String aAccount;
     public ATMFrame(String title){
            super("ATM System - Welcome "+title); 
            aAccount=title;
            main.setLayout(new GridLayout(4,0,30,30));
            main.add(btnNewAccount);
            main.add(btnShowAll);
            main.add(btnExit);

            btnNewAccount.addActionListener(this);
            btnShowAll.addActionListener(this);
            btnExit.addActionListener(this);

            setSize(400,350);      
            setLayout(new FlowLayout());
            add(ATMProject.jPanelIcon, BorderLayout.WEST);
            add(main, BorderLayout.EAST); 
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            setVisible(true);
     }

    public void actionPerformed(ActionEvent evt){
         if(evt.getSource()==btnNewAccount)
               addNewAccount();
        // else if(evt.getSource()==btnSearch)
       //        findBookByISBN();
         else if(evt.getSource()==btnShowAll)
              this.showAllAccounts();
         else
               exitFrame();
     }

     private void addNewAccount(){
          new  NewDialog(this,aAccount,true);
     }

   //  private void findBookByISBN(){
   //        new  FindDialog(this,"Search Book",true);                   
   //  }

     private void showAllAccounts(){
            DisplayDialog display=new  DisplayDialog(this,"Display Accounts",true);
            String output=this.getAccountList();
            JTextArea text=new JTextArea(output);
            text.setEditable(false);
            display.add(text);
            
           
            display.setVisible(true);
     }

     private String getAccountList(){
             Vector list=Banking.showAllAccounts();

             StringBuffer temp=new StringBuffer();
             
             for(int i=0;i<list.size();i++){
               Account account=(Account)list.elementAt(i);
               if (aAccount.equals(account.getUserid())){
                   temp.append(account.toString());  
               }
             }
             return temp.toString();
     }

     private void exitFrame(){
          System.exit(0);
     }

     public static void main(String[] args){
          new  ATMFrame("Simple Bank System");
     }
 }
